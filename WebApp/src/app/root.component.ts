import {Component} from '@angular/core';

@Component({
  selector: 'root',
  template: require('./root.component.html')
})
export class RootComponent {}