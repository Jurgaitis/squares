import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import { PaginationModule } from 'ng2-bootstrap/components/pagination';

import {WebSocketService} from './websockets.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    PaginationModule
  ],
  declarations: [
  ],
  exports: [
  ],
  providers:[
      WebSocketService
  ]
})
export class SharedModule {}