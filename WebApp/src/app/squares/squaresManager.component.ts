import {Component} from '@angular/core';

@Component({
  selector: 'squares-manager',
  template: require('./squaresManager.component.html')
})
export class SquaresManagerComponent {
  public currentPointsList: string;

  constructor() {
  }

  onListChanged(name: string){
      this.currentPointsList = name;
  }

  onCountChanged(count: number){
  }
}