import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import { PaginationModule } from 'ng2-bootstrap/components/pagination';

import {PointsModule} from './../points';
import {SharedModule} from './../shared';

import { SquaresManagerComponent } from './squaresManager.component';
import {SquaresListComponent} from './squaresList.component';

import {SquaresService} from './squares.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    PaginationModule,
    PointsModule,
    SharedModule
  ],
  declarations: [
      SquaresManagerComponent,
      SquaresListComponent
  ],
  exports: [
      SquaresManagerComponent
  ],
  providers: [
      SquaresService
  ]
})
export class SquaresModule {}