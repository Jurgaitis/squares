import {Component, Input, SimpleChanges, OnChanges} from '@angular/core';
import {Http} from '@angular/http';
import {SquaresService, Square} from './squares.service';

@Component({
    selector: 'squares-list',
    template: require('./squaresList.component.html')
})
export class SquaresListComponent implements OnChanges {
    @Input('list-name') listName: string;

    public pagination: Pagination = new Pagination([10, 20, 50]);

    public page: Square[] = [];
    public squares: Square[] = [];

    constructor(private http: Http, private squaresService: SquaresService) {
        if (this.listName) {
            squaresService.squares.subscribe(squares => {
                this.squares = squares;
                squaresService.squares.next(this.listName);
            });
            squaresService.squares.next(this.listName);
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        this.pagination = new Pagination([10, 20, 50]);
        if (this.listName) {
            this.squaresService.squares.subscribe(squares => {
                this.squares = squares;
                this.refreshPage();
                this.squaresService.squares.next(this.listName);
            });
            this.squaresService.squares.next(this.listName);
        }
    }

    refreshPage(): void {
        this.page = this.pagination.getPage<Square>(this.squares);
    }

    calculate(): void {
        this.http.post('api/pointslist/' + this.listName + '/calculate', {}).subscribe(response => console.log('calculation started'));
    }

    pageChanged(event) {
        this.pagination.page = event.page;
        this.refreshPage();
    }
}

class Pagination {
    public pageSize: number;
    public page: number = 1;
    public sort: Sort;

    constructor(public sizes: number[]) {
        this.pageSize = sizes[0];
    }

    getPage<T>(allEntries: T[]): T[] {
        if (allEntries.length === 0) {
            return [];
        }
        //changes to lower page if page does not exist with current page parameters
        if (allEntries.length <= (this.page - 1) * this.pageSize) {
            this.page = allEntries.length % this.pageSize === 0 ? Math.floor(allEntries.length / this.pageSize) : Math.floor(allEntries.length / this.pageSize) + 1;
        }
        if (this.sort) {
            // applies sort if property exists
            allEntries = this.applySort<T>(allEntries);
        }
        var toSkip: number = (this.page - 1) * this.pageSize;
        var toTakeUntil: number = allEntries.length - toSkip >= this.pageSize ? +toSkip + +this.pageSize : allEntries.length;
        return allEntries.slice(toSkip, toTakeUntil);
    }

    private applySort<T extends Object>(array: T[]): T[] {
        var propName: string;
        for (var prop in array[0]) {
            if (array[0].hasOwnProperty(prop) && prop === this.sort.property) {
                propName = prop;
                break;
            }
        }
        if (propName) {
            array.sort((a: T, b: T) => {
                var aProp = a[propName];
                var bProp = b[propName];
                var sortValue: number;
                if (aProp < bProp) {
                    sortValue = -1;
                } else if (aProp > bProp) {
                    sortValue = 1;
                } else {
                    sortValue = 0;
                }
                return this.sort.ascending ? sortValue * (-1) : sortValue;
            });
        }
        return array;
    }
}

class Sort {
    public property: string;
    public ascending: boolean = true;
}

class PointsList {
    public points: Point[] = [];

    constructor(public name: string) {

    }
}

class Point {
    constructor(public x: number, public y: number) {

    }

    isValid(): boolean {
        return this.x >= -5000 && this.x <= 5000 && this.y >= -5000 && this.y <= 5000;
    }
}