import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import {WebSocketService } from './../shared/websockets.service';

const URL = 'ws://localhost:5000';

export interface Point {
    x: number,
    y: number
}

export interface Square {
    corners: Point[]
}

@Injectable()
export class SquaresService {
    public squares: Subject<any>;

    constructor(wsService: WebSocketService) {
        this.squares = <Subject<any>>wsService
            .connect(URL)
            .map((response: MessageEvent): Square[] => {
                let data = JSON.parse(response.data);
                return data;
            });
    }
}