import {Component} from '@angular/core';

@Component({
  selector: 'points-manager',
  template: require('./pointsManager.component.html')
})
export class PointsManagerComponent {
  public currentPointsList: string;
  public currentPointsListCount: number;

  constructor() {
  }

  onListChanged(name: string){
      this.currentPointsList = name;
  }

  onCountChanged(count: number){
      this.currentPointsListCount = count;
  }
}