import {Component, EventEmitter, Output, Renderer, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Http} from '@angular/http';

@Component({
    selector: 'points-lists',
    template: require('./pointsLists.component.html')
})
export class PointsListsComponent implements OnChanges, OnInit {
    @Output() onListChanged = new EventEmitter<string>();
    @Input('selected-count') selectedCount: number;
    @Input('action-bar') actionBarEnabled: boolean;

    public currentPointsLists: PointsList[] = [];

    public selectedList: PointsList;

    public newList: PointsList = new PointsList("", 0);
    public newListEditing: boolean = false;

    constructor(private _renderer: Renderer, private http: Http) {
    }

    ngOnInit(){
        this.http.get('api/pointslist').map(response => response.json()).subscribe(response => {
            this.currentPointsLists = response.entries;
            if (this.currentPointsLists.length) {
                this.select(this.currentPointsLists[0]);
            }
        });
    }

    select(pointsList: PointsList) {
        this.onListChanged.emit(pointsList.name);
        this.selectedList = pointsList;
    }

    new(element: HTMLInputElement) {
        if (!this.newListEditing) {
            this.newListEditing = true;
            setTimeout(_ =>
                this._renderer.invokeElementMethod(element, 'focus', []));
        }
        else {
            this.resetNewList();
        }
    }

    resetNewList() {
        this.newListEditing = false;
        this.newList = new PointsList("", 0);
    }

    add() {
        this.http.post('api/pointslist', this.newList).subscribe(() => {
            this.currentPointsLists.unshift(this.newList);
            this.select(this.newList);
            this.resetNewList();
        });
    }

    deleteSelected() {
        if (this.selectedList) {
            this.currentPointsLists.splice(this.currentPointsLists.indexOf(this.selectedList), 1);
            if (this.currentPointsLists.length) {
                this.selectedList = this.currentPointsLists[0];
            }
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.selectedList && this.selectedCount !== undefined) {
            this.selectedList.count = this.selectedCount;
        }
    }
}

class PointsList {
    constructor(public name: string, public count: number) {

    }
}