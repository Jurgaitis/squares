import {Component, Input, SimpleChanges, OnChanges, Renderer, Output, EventEmitter} from '@angular/core';
import {Http} from '@angular/http';

@Component({
    selector: 'points-list',
    template: require('./pointsList.component.html')
})
export class PointsListComponent implements OnChanges {
    @Input('list-name') listName: string;
    @Input('action-bar') actionBarEnabled: boolean;
    @Output() countChanged = new EventEmitter<number>();

    public newPointCreating: boolean = false;
    public newPoint: any = {};

    public currentList: PointsList = new PointsList("");

    public importErrors: ImportErrors = new ImportErrors();

    public pagination: Pagination = new Pagination([10, 20, 50]);

    public page: Point[] = [];

    constructor(private _renderer: Renderer, private http: Http) {
        this.loadPoints();
    }

    delete(point: Point) {
        this.currentList.points.splice(this.currentList.points.indexOf(point), 1);
        this.refreshPage();
        this.countChanged.emit(this.currentList.points.length);
    }

    clear() {
        this.currentList.points = [];
        this.refreshPage();
        this.countChanged.emit(this.currentList.points.length);
    }

    export(linkElement) {
        var content = "";
        this.currentList.points.forEach((point) => {
            content += point.x + " " + point.y + "\r\n";
        });

        var byteContent = new Uint8Array(content.length);
        for (var i = 0; i < content.length; i++)
            byteContent[i] = content.charCodeAt(i);

        setTimeout(_ => {
            //waits for the element to show before trying to focus it
            this._renderer.setElementAttribute(linkElement, "href", URL.createObjectURL(new Blob([byteContent], { type: "text/plain" })));
            this._renderer.setElementAttribute(linkElement, "download", this.currentList.name + ".txt");
            this._renderer.invokeElementMethod(linkElement, 'click', [])
        });
    }

    importFileSelected(fileInput: any) {
        if (fileInput.target.files && fileInput.target.files[0]) {
            var extension = fileInput.target.files[0].name.split('.').pop();
            if (extension.toLowerCase() !== "txt") {
                this.importErrors.invalidFileType = true;
                return;
            }

            //TODO modal with confirmation
            var reader = new FileReader();

            reader.onload = (e: any) => {
                var value: string = e.target.result;
                var rows: string[] = value.split(/\r?\n/g);

                var pointsToAdd: Point[] = [];

                rows.forEach((row) => {
                    if(row === ""){
                        return;
                    }

                    var nonEmptyValues: string[] = [];
                    var splitBySpace = row.split(' ');
                    splitBySpace.forEach(nospace => {
                        if (nospace !== "") {
                            nonEmptyValues.push(nospace);
                        }
                    });
                    if (nonEmptyValues.length === 2) {
                        var x: number = Number(nonEmptyValues[0]);
                        var y: number = Number(nonEmptyValues[1]);

                        if (!isNaN(x) && !isNaN(y)) {
                            if (!pointsToAdd.some(point => point.x === x && point.y === y)) {
                                var newPoint: Point = new Point(x, y);
                                if (newPoint.isValid()) {
                                    pointsToAdd.push(newPoint);
                                }
                                else {
                                    this.importErrors.hadInvalidRows = true;
                                }
                            }
                            else {
                                this.importErrors.hadDuplicateRows = true;
                            }
                        }
                        else {
                            this.importErrors.hadInvalidRows = true;
                        }
                    }
                    else {
                        this.importErrors.hadInvalidRows = true;
                    }
                });
                //TODO check if > 10000

                this.currentList.points = pointsToAdd;
                this.refreshPage();
                this.countChanged.emit(this.currentList.points.length);
                fileInput.target.value = null;
            }
            reader.readAsText(fileInput.target.files[0]);
        }
    }

    new(element: HTMLInputElement) {
        if (!this.newPointCreating) {
            this.newPointCreating = true;
            setTimeout(_ =>
                //waits for the element to show before trying to focus it
                this._renderer.invokeElementMethod(element, 'focus', []));
        }
        else {
            this.resetNewPoint();
        }
    }

    add() {
        this.currentList.points.unshift(this.newPoint);
        this.countChanged.emit(this.currentList.points.length);
        this.resetNewPoint();
        this.refreshPage();
    }

    resetNewPoint() {
        this.newPoint = {};
        this.newPointCreating = false;
    }

    ngOnChanges(changes: SimpleChanges) {
        this.pagination = new Pagination([10, 20, 50]);
        this.loadPoints();
    }

    save() {
        this.http.post('api/pointslist', this.currentList).subscribe(() => console.log(this.currentList.name + ' saved'));
    }

    loadPoints() {
        if (this.listName) {
            this.http.get('api/pointslist/' + this.listName + '/points')
                .map(response => response.json())
                .subscribe(response => {
                    this.currentList = new PointsList(this.listName);
                    this.currentList.points = response.entries;
                    this.refreshPage();
                });
        }
    }

    refreshPage(): void {
        this.page = this.pagination.getPage<Point>(this.currentList.points);
    }

    pageChanged(event) {
        this.pagination.page = event.page;
        this.refreshPage();
    }

    sort(property: string) {
        var normalizedProp = property.toLowerCase();
        // changes direction if sorting by the same property
        if (this.pagination.sort && this.pagination.sort.property === normalizedProp) {
            this.pagination.sort.ascending = !this.pagination.sort.ascending;
        }
        else {
            this.pagination.sort = new Sort();
            this.pagination.sort.property = property;
        }
        this.refreshPage();
    }
}

class Pagination {
    public pageSize: number;
    public page: number = 1;
    public sort: Sort;

    constructor(public sizes: number[]) {
        this.pageSize = sizes[0];
    }

    getPage<T>(allEntries: T[]): T[] {
        if (allEntries.length === 0) {
            return [];
        }
        //changes to lower page if page does not exist with current page parameters
        if (allEntries.length <= (this.page - 1) * this.pageSize) {
            this.page = allEntries.length % this.pageSize === 0 ? Math.floor(allEntries.length / this.pageSize) : Math.floor(allEntries.length / this.pageSize) + 1;
        }
        if (this.sort) {
            // applies sort if property exists
            allEntries = this.applySort<T>(allEntries);
        }
        var toSkip: number = (this.page - 1) * this.pageSize;
        var toTakeUntil: number = allEntries.length - toSkip >= this.pageSize ? +toSkip + +this.pageSize : allEntries.length;
        return allEntries.slice(toSkip, toTakeUntil);
    }

    private applySort<T extends Object>(array: T[]): T[] {
        var propName: string;
        for (var prop in array[0]) {
            if (array[0].hasOwnProperty(prop) && prop === this.sort.property) {
                propName = prop;
                break;
            }
        }
        if (propName) {
            array.sort((a: T, b: T) => {
                var aProp = a[propName];
                var bProp = b[propName];
                var sortValue: number;
                if (aProp < bProp) {
                    sortValue = -1;
                } else if (aProp > bProp) {
                    sortValue = 1;
                } else {
                    sortValue = 0;
                }
                return this.sort.ascending ? sortValue * (-1) : sortValue;
            });
        }
        return array;
    }
}

class Sort {
    public property: string;
    public ascending: boolean = true;
}

class ImportErrors {
    public hadInvalidRows: boolean = false;
    public hadDuplicateRows: boolean = false;
    public hadTooManyRows: boolean = false;
    public invalidFileType: boolean = false;

    hasErrors(): boolean {
        for (var value in this) {
            if (this.hasOwnProperty(value) && this[value]) {
                return true;
            }
        }
        return false;
    }
}

class PointsList {

    public points: Point[] = [];

    constructor(public name: string) {

    }
}

class Point {
    constructor(public x: number, public y: number) {

    }

    isValid(): boolean {
        return this.x >= -5000 && this.x <= 5000 && this.y >= -5000 && this.y <= 5000;
    }
}