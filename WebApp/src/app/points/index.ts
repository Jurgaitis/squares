import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import { PaginationModule } from 'ng2-bootstrap/components/pagination';

import {PointsManagerComponent} from './pointsManager.component';
import {PointsListsComponent} from './pointsLists.component';
import {PointsListComponent} from './pointsList.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    PaginationModule
  ],
  declarations: [
      PointsManagerComponent,
      PointsListsComponent,
      PointsListComponent
  ],
  exports: [
      PointsManagerComponent,
      PointsListsComponent
  ]
})
export class PointsModule {}