/// <reference path="../../typings/index.d.ts"/>

import {RouterModule, Routes} from '@angular/router';
import {PointsManagerComponent} from './points/pointsManager.component';
import { SquaresManagerComponent } from './squares/squaresManager.component';

export const routes: Routes = [
  {
    path: 'points',
    component: PointsManagerComponent
  },
  {
    path: 'squares',
    component: SquaresManagerComponent
  },
  {
    path: '',
    redirectTo: 'points',
    pathMatch: 'full'
  }
];

export const routing = RouterModule.forRoot(routes);
