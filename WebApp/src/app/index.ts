import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {routing} from './routes';

import {PointsModule} from './points';
import {SquaresModule} from './squares';
import {SharedModule} from './shared';

import {RootComponent} from './root.component';

@NgModule({
  imports: [
    BrowserModule,
    routing,
    PointsModule,
    SquaresModule,
    SharedModule
  ],
  declarations: [
    RootComponent
  ],
  bootstrap: [RootComponent]
})
export class AppModule {}
