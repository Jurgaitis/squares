const conf = require('./gulp.conf');
var proxyMiddleware = require('http-proxy-middleware');

module.exports = function () {
  var proxy = proxyMiddleware('/api',
    { 
      target: 'http://localhost:5000', 
      logLevel: 'debug', 
      changeOrigin: true,
      // pathRewrite: {
      //   '^api': ''
      // }
    });
  return {
    server: {
      baseDir: [
        conf.paths.tmp,
        conf.paths.src
      ],
      middleware: [proxy]
    },
    open: false
  };
};
