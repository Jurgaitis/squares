﻿using System.Collections.Generic;
using System.Linq;

namespace Squares.Domain.Utils
{
    public static class EnumerableUtils
    {
        public static bool UnorderedEquals<T>(this IEnumerable<T> one, IEnumerable<T> two)
        {
            if (one.Count() != two.Count())
            {
                return false;
            }

            var first = one.GroupBy(t => t).ToDictionary(group => group.Key, group => group.Count());
            var second = two.GroupBy(t => t).ToDictionary(group => group.Key, group => group.Count());

            if (first.Count != second.Count)
            {
                return false;
            }

            foreach (var f in first)
            {
                var secondValue = second.SingleOrDefault(a => a.Key.Equals(f.Key));

                if (object.Equals(null, secondValue) || f.Value != secondValue.Value)
                {
                    return false;
                }
            }

            return true;
        }
    }
}