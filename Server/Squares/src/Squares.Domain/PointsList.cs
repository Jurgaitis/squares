﻿using System;
using System.Collections.Generic;

namespace Squares.Domain
{
    /// <summary>
    /// Points list with name
    /// </summary>
    public abstract class PointsList
    {
        protected PointsList(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }
            Name = name;
        }
        /// <summary>
        /// Name of the list.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Read only points list.
        /// </summary>
        public virtual IReadOnlyList<Point> Points { get; }
        /// <summary>
        /// Adds new point to the list.
        /// </summary>
        /// <param name="point">Point to add.</param>
        public abstract void AddNewPoint(Point point);

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is PointsList) || obj.GetType() != this.GetType())
            {
                return false;
            }

            var pointList = obj as PointsList;

            return ReferenceEquals(this, obj) || pointList.Name.Equals(Name);
        }

        public override int GetHashCode()
        {
            int hash = 13;

            hash = hash * 7 + Name.GetHashCode();

            return hash;
        }

        public static bool operator ==(PointsList one, PointsList two)
        {
            if (ReferenceEquals(null, one) && ReferenceEquals(null, two))
            {
                return true;
            }
            if (ReferenceEquals(null, one) || ReferenceEquals(null, two))
            {
                return false;
            }

            return Equals(one, two);
        }

        public static bool operator !=(PointsList one, PointsList two)
        {
            return !(one == two);
        }
    }
}