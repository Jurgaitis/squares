﻿using System.Collections.Generic;

namespace Squares.Domain
{
    /// <summary>
    /// Points list repository
    /// </summary>
    public interface IPointsListRepository
    {
        /// <summary>
        /// Saves points list.
        /// </summary>
        /// <param name="pointsList">Points list to save.</param>
        void Create(PointsList pointsList);
        /// <summary>
        /// Gets points list by the specified name.
        /// </summary>
        /// <param name="name">Name to get the points list with.</param>
        /// <returns>Points list.</returns>
        PointsList Get(string name);
        /// <summary>
        /// Get all points lists.
        /// </summary>
        /// <returns>Points lists.</returns>
        IEnumerable<PointsList> Get();
        /// <summary>
        /// Deletes saved points list by its name.
        /// </summary>
        /// <param name="name">Name of the points list.</param>
        void Delete(string name);
    }
}