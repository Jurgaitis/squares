﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Squares.Domain.Internal
{
    internal class NoDuplicatesPointsList : PointsList
    {
        public NoDuplicatesPointsList(string name) : base(name)
        {
        }

        private List<Point> _points = new List<Point>();
        public override IReadOnlyList<Point> Points => _points.AsReadOnly();

        public override void AddNewPoint(Point point)
        {
            if (point == null)
            {
                throw new ArgumentNullException(nameof(point));
            }
            if (_points.Any(_point => _point == point))
            {
                throw new InvalidOperationException($"Point({point.X}, {point.Y}) already exists in the list.");
            }
            if (_points.Count == 10000)
            {
                throw new InvalidOperationException("Point list is full.");
            }

            _points.Add(point);
        }
    }
}