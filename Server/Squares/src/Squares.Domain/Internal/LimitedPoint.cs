﻿using System;

namespace Squares.Domain.Internal
{
    internal class LimitedPoint : Point
    {
        public LimitedPoint(int x, int y)
        {
            if (x > 5000 || x < -5000)
            {
                throw new ArgumentOutOfRangeException(nameof(x));
            }
            if (y > 5000 || y < -5000)
            {
                throw new ArgumentOutOfRangeException(nameof(y));
            }

            X = x;
            Y = y;
        }

        public override int X { get; }
        public override int Y { get; }
    }
}