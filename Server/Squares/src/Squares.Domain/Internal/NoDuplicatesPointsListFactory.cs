﻿namespace Squares.Domain.Internal
{
    internal class NoDuplicatesPointsListFactory : IPointsListFactory
    {
        public PointsList Create(string name)
        {
            return new NoDuplicatesPointsList(name);
        }
    }
}