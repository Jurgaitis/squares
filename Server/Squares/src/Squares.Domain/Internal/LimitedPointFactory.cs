﻿namespace Squares.Domain.Internal
{
    internal class LimitedPointFactory : IPointFactory
    {
        public Point Create(int x, int y)
        {
            return new LimitedPoint(x, y);
        }
    }
}