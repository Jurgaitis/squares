﻿namespace Squares.Domain
{
    /// <summary>
    /// Points factory.
    /// </summary>
    public interface IPointFactory
    {
        /// <summary>
        /// Creates new point from specified coordinates.
        /// </summary>
        /// <param name="x">X coordinate value of the point.</param>
        /// <param name="y">Y coordinate value of the point.</param>
        /// <returns>new point</returns>
        Point Create(int x, int y);
    }
}