﻿using Squares.Domain;
using Squares.Domain.Internal;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class PointServiceCollectionExtensions
    {
        public static IServiceCollection AddPointServices(this IServiceCollection services)
        {
            services.AddSingleton<IPointFactory, LimitedPointFactory>();
            services.AddSingleton<IPointsListFactory, NoDuplicatesPointsListFactory>();

            return services;
        }
    }
}