﻿using System;
using Squares.Domain.Utils;
using System.Collections.Generic;

namespace Squares.Domain.Calculation
{
    public abstract class Polygon : IEquatable<Polygon>
    {
        public abstract IReadOnlyList<Point> Corners { get; }

        public bool Equals(Polygon other)
        {
            return Equals(other as object);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Polygon) || obj.GetType() != this.GetType())
            {
                return false;
            }

            var polygon = obj as Polygon;

            return ReferenceEquals(this, obj) || polygon.Corners.UnorderedEquals(Corners);
        }

        public override int GetHashCode()
        {
            int hash = 13;
            for (var i = 0; i < Corners.Count; i++)
            {
                hash = hash * 7 + Corners[i].GetHashCode() + i.GetHashCode();
            }

            return hash;
        }

        public static bool operator ==(Polygon one, Polygon two)
        {
            if (ReferenceEquals(null, one) && ReferenceEquals(null, two))
            {
                return true;
            }
            if (ReferenceEquals(null, one) || ReferenceEquals(null, two))
            {
                return false;
            }

            return Equals(one, two);
        }

        public static bool operator !=(Polygon one, Polygon two)
        {
            return !(one == two);
        }
    }
}