﻿using Squares.Domain.Calculation.Squares;
using Squares.Domain.Calculation.Squares.Internal;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class SquareCalculationServiceCollectionExtensions
    {
        public static IServiceCollection AddSquareCalculation(this IServiceCollection services)
        {
            services.AddPointServices();

            services.AddSingleton<ISquareBuilderFactory, SquareBuilderFactory>();

            services.AddScoped<ISquaresCalculator, SquaresCalculator>();

            return services;
        }
    }
}