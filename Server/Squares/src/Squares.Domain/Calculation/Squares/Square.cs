﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Squares.Domain.Calculation.Squares
{
    public class Square : Polygon
    {
        public Square(IEnumerable<Point> corners)
        {
            if (corners == null)
            {
                throw new ArgumentNullException(nameof(corners));
            }
            var enumeratedCorners = corners as IList<Point> ?? corners.ToList();
            if(enumeratedCorners.Count() != 4)
            {
                throw new ArgumentException(nameof(corners));
            }
            _corners.AddRange(enumeratedCorners);
        }

        private readonly List<Point> _corners = new List<Point>(4);
        public override IReadOnlyList<Point> Corners => _corners.AsReadOnly();
    }
}