﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Squares.Domain.Calculation.Squares.Internal
{
    internal class SquareBuilder : ISquareBuilder
    {
        private readonly IPointFactory _pointFactory;

        private readonly List<Point> _corners = new List<Point>(4);
        private readonly List<Square> _possibleSquares = new List<Square>(3);

        public SquareBuilder(IPointFactory pointFactory)
        {
            _pointFactory = pointFactory;
        }

        private SquareBuilder(List<Point> corners, List<Square> possibleSquares, IPointFactory _factory)
        {
            _corners = corners;
            _possibleSquares = possibleSquares;
            Full = _corners.Count == 4;
            _pointFactory = _factory;
        }

        public bool Full { get; private set; }

        public bool IsValid(Point point)
        {
            return _corners.All(corner => corner != point) &&
                   (_possibleSquares.Count == 0 || IsInAnyPossibleSquare(point));
        }

        public bool TryAdd(Point point)
        {
            if (point == null)
            {
                throw new ArgumentNullException(nameof(point));
            }

            return _corners.All(corner => corner != point) && TryAddToTheList(point);
        }

        public Square Build()
        {
            if (!Full)
            {
                throw new InvalidOperationException("Missing points.");
            }

            return new Square(_corners);
        }

        public ISquareBuilder Clone()
        {
            var corners = new Point[_corners.Count];
            var squares = new Square[_possibleSquares.Count];

            _corners.CopyTo(corners);
            _possibleSquares.CopyTo(squares);

            return new SquareBuilder(_corners.ToList(), squares.ToList(), _pointFactory);
        }

        private bool TryAddToTheList(Point point)
        {
            var cornersCount = _corners.Count;

            switch (cornersCount)
            {
                case 0:
                    _corners.Add(point);
                    return true;
                case 1:
                    if (_corners.Single() != point)
                    {
                        _corners.Add(point);
                        CalculatePossibleSquares();
                        return true;
                    }
                    return false;
                case 2:
                    if (IsInAnyPossibleSquare(point))
                    {
                        _corners.Add(point);
                        ClearNotContainingSquares(point);
                        return true;
                    }
                    return false;
                case 3:
                    if (IsInAnyPossibleSquare(point))
                    {
                        _corners.Add(point);
                        Full = true;
                        return true;
                    }
                    return false;
                default:
                    throw new InvalidOperationException("Cannot add any more points since builder is full.");
            }
        }

        private void CalculatePossibleSquares()
        {
            var pointA = _corners.First();
            var pointB = _corners.Skip(1).Single();

            var diagnolSquare = CalculateWhenDiagnollyOposite(pointA, pointB);
            if (diagnolSquare != null)
            {
                _possibleSquares.Add(diagnolSquare);
            }

            var squareOnOneSide = CalculateToTheSide(pointA, pointB);
            if (squareOnOneSide != null)
            {
                _possibleSquares.Add(squareOnOneSide);
            }

            var squareOnOtherSide = CalculateToTheSide(pointB, pointA);
            if (squareOnOtherSide != null)
            {
                _possibleSquares.Add(squareOnOtherSide);
            }
        }

        private Square CalculateToTheSide(Point pointA, Point pointB)
        {
            Point pointC;
            Point pointD;
            try
            {
                pointC = _pointFactory.Create(pointA.X + pointB.Y - pointA.Y, pointA.Y + pointA.X - pointB.X);
                pointD = _pointFactory.Create(pointB.X + pointB.Y - pointA.Y, pointB.Y + pointA.X - pointB.X);
            }
            catch (ArgumentException)
            {
                return null;
            }

            var corners = new Point[4];
            _corners.CopyTo(corners);
            corners[2] = pointC;
            corners[3] = pointD;

            return new Square(corners);
        }

        private Square CalculateWhenDiagnollyOposite(Point pointA, Point pointB)
        {
            var pointCX = (pointA.X + pointB.X + pointB.Y - pointA.Y) / 2;
            if (!IsInteger(pointCX))
            {
                return null;
            }
            var pointCY = (pointA.Y + pointB.Y + pointA.X - pointB.X) / 2;
            if (!IsInteger(pointCY))
            {
                return null;
            }
            var pointDX = (pointA.X + pointB.X - pointB.Y + pointA.Y) / 2;
            if (!IsInteger(pointDX))
            {
                return null;
            }
            var pointDY = (pointA.Y + pointB.Y - pointA.X + pointB.X) / 2;
            if (!IsInteger(pointDY))
            {
                return null;
            }

            Point pointC;
            Point pointD;
            try
            {
                pointC = _pointFactory.Create(pointCX, pointCY);
                pointD = _pointFactory.Create(pointDX, pointDY);
            }
            catch (ArgumentException)
            {
                return null;
            }

            var corners = new Point[4];
            _corners.CopyTo(corners);
            corners[2] = pointC;
            corners[3] = pointD;

            return new Square(corners);
        }

        //TODO move to utils
        private bool IsInteger(double number)
        {
            return Math.Abs(number - Math.Truncate(number)) < 0.00000001;
        }

        private void ClearNotContainingSquares(Point point)
        {
            _possibleSquares.RemoveAll(square => square.Corners.All(corner => corner != point));
        }

        private bool IsInAnyPossibleSquare(Point point)
        {
            return _possibleSquares.Any(square => square.Corners.Any(corner => corner == point));
        }
    }
}