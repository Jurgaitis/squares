﻿namespace Squares.Domain.Calculation.Squares.Internal
{
    internal class SquareBuilderFactory : ISquareBuilderFactory
    {
        private readonly IPointFactory _pointFactory;

        public SquareBuilderFactory(IPointFactory pointFactory)
        {
            _pointFactory = pointFactory;
        }

        public ISquareBuilder Create()
        {
            return new SquareBuilder(_pointFactory);
        }
    }
}