﻿using System;

namespace Squares.Domain.Calculation.Squares.Internal
{
    internal class SquaresCalculatorFacotry : ISquaresCalculatorFactory
    {
        private readonly ISquareBuilderFactory _squareBuilderFactory;

        public SquaresCalculatorFacotry(ISquareBuilderFactory squareBuilderFactory)
        {
            _squareBuilderFactory = squareBuilderFactory;
        }

        public ISquaresCalculator Create(Action<Square> onFound)
        {
            return new SquaresCalculator(_squareBuilderFactory);
        }
    }
}