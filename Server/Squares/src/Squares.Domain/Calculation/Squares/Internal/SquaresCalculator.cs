﻿using System;
using System.Threading.Tasks;

namespace Squares.Domain.Calculation.Squares.Internal
{
    internal class SquaresCalculator : ISquaresCalculator
    {
        private readonly ISquareBuilderFactory _factory;

        private Object thisLock = new Object();

        public SquaresCalculator(ISquareBuilderFactory factory)
        {
            if (factory == null)
            {
                throw new ArgumentNullException(nameof(factory));
            }
            _factory = factory;
        }

        public void Calculate(PointsList pointList)
        {
            var builder = _factory.Create();

            CheckAllPoints(pointList, builder);
        }

        private void CheckAllPoints(PointsList pointList, ISquareBuilder builder)
        {
            var points = pointList.Points;

            foreach (var point in points)
            {
                if (!builder.IsValid(point)) continue;

                var newBuilder = builder.Clone();
                newBuilder.TryAdd(point);

                if (newBuilder.Full)
                {
                    ProcessSquare(newBuilder.Build());
                }
                CheckAllPoints(pointList, newBuilder);
            }
        }

        private void ProcessSquare(Square square)
        {
            if (OnFound == null)
            {
                throw new InvalidOperationException("You must set onFound callback first.");
            }

            lock (thisLock)
            {
                OnFound(square);
            }
        }

        // TODO move to options
        public Action<Square> OnFound { get; set; }
    }
}