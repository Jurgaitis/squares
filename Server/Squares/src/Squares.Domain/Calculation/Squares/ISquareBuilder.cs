﻿namespace Squares.Domain.Calculation.Squares
{
    internal interface ISquareBuilder : IPolygonBuilder<Square>
    {
        ISquareBuilder Clone();
    }
}