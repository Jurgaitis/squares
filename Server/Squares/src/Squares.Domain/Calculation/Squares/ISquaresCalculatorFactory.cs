﻿using System;

namespace Squares.Domain.Calculation.Squares
{
    public interface ISquaresCalculatorFactory
    {
        ISquaresCalculator Create(Action<Square> onFound);
    }
}