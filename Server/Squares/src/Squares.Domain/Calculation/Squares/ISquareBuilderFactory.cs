﻿namespace Squares.Domain.Calculation.Squares
{
    internal interface ISquareBuilderFactory
    {
        ISquareBuilder Create();
    }
}