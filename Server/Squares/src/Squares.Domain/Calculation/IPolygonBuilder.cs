﻿namespace Squares.Domain.Calculation
{
    public interface IPolygonBuilder<out T> where T : Polygon
    {
        bool Full { get; }
        bool TryAdd(Point point);
        bool IsValid(Point point);
        T Build();
    }
}