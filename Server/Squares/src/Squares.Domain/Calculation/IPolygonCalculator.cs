﻿using System;
using System.Threading.Tasks;

namespace Squares.Domain.Calculation
{
    public interface IPolygonCalculator<T> where T : Polygon
    {
        void Calculate(PointsList pointList);

        Action<T> OnFound { get; set; }
    }
}