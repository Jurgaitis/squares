﻿namespace Squares.Domain
{
    /// <summary>
    /// Point list factory.
    /// </summary>
    public interface IPointsListFactory
    {
        /// <summary>
        /// Creates new points list with specified name.
        /// </summary>
        /// <param name="name">Name of the points list.</param>
        /// <returns>points list</returns>
        PointsList Create(string name);
    }
}