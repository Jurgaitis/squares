﻿using System;

namespace Squares.Domain
{
    /// <summary>
    /// Two dimensional point
    /// </summary>
    public abstract class Point
    {
        protected Point()
        {
        }

        protected Point(int x, int y)
        {
            X = x;
            Y = y;
        }
        /// <summary>
        /// Horizontal coordinate
        /// </summary>
        public virtual int X { get; }
        /// <summary>
        /// Vertical coordinate
        /// </summary>
        public virtual int Y { get; }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Point) || obj.GetType() != this.GetType())
            {
                return false;
            }

            var point = obj as Point;

            return ReferenceEquals(this, obj) || (X == point.X && Y == point.Y);
        }

        public override int GetHashCode()
        {
            int hash = 13;

            hash = hash * 7 + X.GetHashCode();
            hash = hash * 7 + Y.GetHashCode();

            return hash;
        }

        public static bool operator ==(Point one, Point two)
        {
            if (ReferenceEquals(null, one) && ReferenceEquals(null, two))
            {
                return true;
            }
            if (ReferenceEquals(null, one) || ReferenceEquals(null, two))
            {
                return false;
            }

            return Equals(one, two);
        }

        public static bool operator !=(Point one, Point two)
        {
            return !(one == two);
        }
    }
}
