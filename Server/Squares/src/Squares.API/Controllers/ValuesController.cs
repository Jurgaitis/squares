﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Squares.Domain;
using Squares.Domain.Calculation.Squares;
using System.Linq;

namespace Squares.API.Controllers
{
    [Route("[controller]")]
    public class SquaresController : Controller
    {
        private readonly IPointsListFactory _pointsListFactory;
        private readonly IPointFactory _pointsFactory;
        private readonly ISquaresCalculatorFactory _calculatorFacotry;

        public SquaresController(IPointsListFactory pointsListFactory, IPointFactory pointsFactory,
            ISquaresCalculatorFactory calculatorFacotry)
        {
            _pointsListFactory = pointsListFactory;
            _pointsFactory = pointsFactory;
            _calculatorFacotry = calculatorFacotry;
        }

        [HttpGet]
        public ActionResult Get()
        {
            var list = _pointsListFactory.Create("name");
            list.AddNewPoint(_pointsFactory.Create(0, 0));
            list.AddNewPoint(_pointsFactory.Create(0, 10));
            list.AddNewPoint(_pointsFactory.Create(5, 5));
            list.AddNewPoint(_pointsFactory.Create(-5, 5));
            list.AddNewPoint(_pointsFactory.Create(10, 10));
            list.AddNewPoint(_pointsFactory.Create(10, 0));

            var foundSquares = new List<Square>();

            var calcualtor = _calculatorFacotry.Create((square) =>
            {
                if (foundSquares.All(sqr => sqr != square))
                {
                    foundSquares.Add(square);
                }
            });

            calcualtor.Calculate(list);

            return new OkObjectResult(foundSquares);
        }
    }
}
