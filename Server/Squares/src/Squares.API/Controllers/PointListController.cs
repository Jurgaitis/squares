﻿using Microsoft.AspNetCore.Mvc;
using Squares.API.ViewModels;
using Squares.Domain;
using System.Linq;
using Squares.API.Pagination;
using Squares.API.Pagination.Configuration;
using Squares.API.TaskManager;
using Squares.API.ViewModels.Persistence;
using Squares.Domain.Calculation.Squares;

namespace Squares.API.Controllers
{
    [Route("[controller]")]
    public class PointsListController : Controller
    {
        private readonly IPointsListFactory _pointsListFactory;
        private readonly IPointsListRepository _pointsListRepository;
        private readonly IPointListViewModelRepository _pointsListViewModelRepository;
        private readonly IPointFactory _pointFactory;

        private readonly ICalculationManager<Square> _calcualtionsManager;
        private readonly ISquaresCalculator _calculator;

        public PointsListController(IPointsListFactory pointsListFactory, IPointFactory pointFactory,
            IPointsListRepository pointsListRepository, IPointListViewModelRepository pointsListViewModelRepository,
            ISquaresCalculator calculator, ICalculationManager<Square> calcualtionsManager)
        {
            _pointsListFactory = pointsListFactory;
            _pointFactory = pointFactory;
            _pointsListRepository = pointsListRepository;
            _pointsListViewModelRepository = pointsListViewModelRepository;
            _calculator = calculator;
            _calcualtionsManager = calcualtionsManager;
        }

        [HttpPost]
        public ActionResult Create([FromBody] PointsListCreateViewModel pointListModel)
        {
            //TODO move to mapper service like Automapper
            var pointsList = _pointsListFactory.Create(pointListModel.Name);
            pointListModel.Points.ForEach(point => pointsList.AddNewPoint(_pointFactory.Create(point.X, point.Y)));

            _pointsListRepository.Create(pointsList);

            return new CreatedAtRouteResult("Get", new {name = pointsList.Name}, pointsList);
        }

        [HttpGet("{name}", Name = "Get")]
        public ActionResult Get(string name)
        {
            var pointsList = _pointsListRepository.Get(name);
            if (pointsList == null)
            {
                return new NotFoundResult();
            }
            var model = new PointsListDetailsViewModel
            {
                Name = pointsList.Name,
                Points = pointsList.Points.Select(point => new PointViewModel
                {
                    X = point.X,
                    Y = point.Y
                }).ToList()
            };
            return new OkObjectResult(model);
        }

        [HttpGet]
        public ActionResult Get([ModelBinder(BinderType = typeof(PaginationModelBinder))] PaginationParameters parameters)
        {
            var pointsList = _pointsListViewModelRepository.GetPage(parameters);
            return new OkObjectResult(pointsList);
        }

        [HttpGet("{name}/points")]
        public ActionResult GetPoints(
            [ModelBinder(BinderType = typeof(PaginationModelBinder))] PaginationParameters parameters, string name)
        {
            var points = _pointsListViewModelRepository.GetPointsPage(name, parameters);
            return new OkObjectResult(points);
        }

        [HttpDelete("{name}")]
        public ActionResult Delete(string name)
        {
            _pointsListRepository.Delete(name);

            return new OkResult();
        }

        [HttpPost("{name}/calculate")]
        public ActionResult Calculate(string name)
        {
            var pointsList = _pointsListRepository.Get(name);

            _calcualtionsManager.Calculate(pointsList, _calculator);

            return new OkResult();
        }
    }
}