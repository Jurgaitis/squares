﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Squares.API.TaskManager;
using Squares.Domain.Calculation.Squares;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Squares.API
{
    public class WebsocketMiddleware
    {
        private RequestDelegate _next;

        public WebsocketMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.WebSockets.IsWebSocketRequest)
            {
                var webSocket = await context.WebSockets.AcceptWebSocketAsync();

                var calculationManager = context.RequestServices.GetService<ICalculationManager<Square>>();
                var buffer = new byte[1024 * 4];
                var result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                while (!result.CloseStatus.HasValue)
                {
                    // If the client send "ServerClose", then they want a server-originated close to occur
                    string content = "<<binary>>";
                    if (result.MessageType == WebSocketMessageType.Text)
                    {
                        content = Encoding.UTF8.GetString(buffer, 0, result.Count);
                        if (content.Equals("ServerClose"))
                        {
                            await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing from Server", CancellationToken.None);
                            return;
                        }
                        if (content.Equals("ServerAbort"))
                        {
                            context.Abort();
                        }
                        var squares =
                            calculationManager.GetCalculated(JsonConvert.DeserializeObject<string>(content));
                        var response = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(squares ?? new List<Square>(), new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        }));
                        await webSocket.SendAsync(new ArraySegment<byte>(response, 0, response.Length), result.MessageType, result.EndOfMessage, CancellationToken.None);
                        //if (squares == null || calculationManager.IsCalculationSuccessful(content))
                        //{
                        //    await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing from Server", CancellationToken.None);
                        //}
                    }

                    result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                }
                await webSocket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
            }
            else
            {
                await _next.Invoke(context);
            }
        }
    }
}