﻿using System.ComponentModel.DataAnnotations;

namespace Squares.API.ViewModels
{
    public class PointViewModel
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}