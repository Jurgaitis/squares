﻿namespace Squares.API.ViewModels
{
    public class PointListListViewModel
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}