﻿using System.Collections.Generic;

namespace Squares.API.ViewModels
{
    public class PointsListDetailsViewModel
    {
        public string Name { get; set; }
        public List<PointViewModel> Points { get; set; }
    }
}