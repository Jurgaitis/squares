﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Squares.API.ViewModels
{
    public class PointsListCreateViewModel : IValidatableObject
    {
        [Required, StringLength(100, MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        [MaxLength(10000)]
        public List<PointViewModel> Points { get; set; } = new List<PointViewModel>();

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Points.GroupBy(point => point, new PointViewModelEqualityComparer()).Any(group => group.Count() > 1))
            {
                yield return new ValidationResult("List contains duplicate points.");
            }
        }

        public class PointViewModelEqualityComparer : IEqualityComparer<PointViewModel>
        {
            public bool Equals(PointViewModel one, PointViewModel two)
            {
                return one.X == two.X && one.Y == two.Y;
            }

            public int GetHashCode(PointViewModel obj)
            {
                int hash = 13;
                hash = hash * 7 + obj.X;
                hash = hash * 7 + obj.Y;

                return hash;
            }
        }
    }
}