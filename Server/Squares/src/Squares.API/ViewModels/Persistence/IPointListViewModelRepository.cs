﻿using Squares.API.Pagination;

namespace Squares.API.ViewModels.Persistence
{
    public interface IPointListViewModelRepository
    {
        PageModel<PointListListViewModel> GetPage(PaginationParameters parameters);
        PageModel<PointViewModel> GetPointsPage(string name, PaginationParameters parameters);
    }
}