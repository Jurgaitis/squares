﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Squares.Domain.Calculation.Squares;
using System.Threading;
using Squares.Domain;
using Squares.Domain.Calculation;
using System.Linq;

namespace Squares.API.TaskManager.Internal
{
    public abstract class CalculationManager<T> : ICalculationManager<T> where T : Polygon
    {
        private Dictionary<string, CalculationModel<T>> _squaresCache = new Dictionary<string, CalculationModel<T>>();

        public void Calculate(PointsList pointsList, IPolygonCalculator<T> calculator)
        {
            CalculationModel<T> cache;
            if (_squaresCache.TryGetValue(pointsList.Name, out cache))
            {
                cache.CancellationTokenSource.Cancel();
                _squaresCache.Remove(pointsList.Name);
                cache.Dispose();
            }

            StartNewCalculation(pointsList, calculator);
        }

        private void StartNewCalculation(PointsList pointsList, IPolygonCalculator<T> calculator)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            var calculationTask = new Task(() => calculator.Calculate(pointsList), cancellationTokenSource.Token);

            var calculationModel = new CalculationModel<T>(calculationTask, cancellationTokenSource);

            calculator.OnFound = polygon =>
            {
                if (calculationModel.FoundPolygons.All(pol => pol != polygon))
                {
                    calculationModel.FoundPolygons.Add(polygon);
                }
            };

            _squaresCache.Add(pointsList.Name, calculationModel);

            calculationModel.CalculationTask.Start();
        }

        public bool IsCalculationSuccessful(string pointsListName)
        {
            CalculationModel<T> cache;
            if (!_squaresCache.TryGetValue(pointsListName, out cache))
            {
                return true;
            }

            return cache.CalculationTask.IsCompleted;
        }

        public IEnumerable<T> GetCalculated(string pointsListName)
        {
            CalculationModel<T> cache;
            if (!_squaresCache.TryGetValue(pointsListName, out cache))
            {
                return null;
            }

            return cache.FoundPolygons;
        }
    }

    internal class CalculationModel<T> : IDisposable
    {
        public CalculationModel(Task calculationTask, CancellationTokenSource cancellationTokenSource)
        {
            if (calculationTask == null)
            {
                throw new ArgumentNullException(nameof(calculationTask));
            }
            CalculationTask = calculationTask;
            CancellationTokenSource = cancellationTokenSource;
        }

        public Task CalculationTask { get; }
        public CancellationTokenSource CancellationTokenSource { get; }
        public List<T> FoundPolygons { get; } = new List<T>();
        public void Dispose()
        {
            CancellationTokenSource.Dispose();
        }
    }
}