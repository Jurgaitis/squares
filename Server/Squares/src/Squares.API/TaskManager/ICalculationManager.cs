﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Squares.Domain;
using Squares.Domain.Calculation;
using Squares.Domain.Calculation.Squares;

namespace Squares.API.TaskManager
{
    public interface ICalculationManager<T> where T : Polygon
    {
        void Calculate(PointsList pointsList, IPolygonCalculator<T> calculator);
        bool IsCalculationSuccessful(string pointsListName);
        IEnumerable<T> GetCalculated(string pointsListName);
    }
}