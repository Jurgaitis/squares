﻿using System.Collections.Generic;

namespace Squares.API.Pagination
{
    public class PageModel<T>
    {
        public int TotalEntriesCount { get; set; }
        public IEnumerable<T> Entries { get; set; }
    }
}