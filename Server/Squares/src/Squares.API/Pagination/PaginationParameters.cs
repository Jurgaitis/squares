﻿namespace Squares.API.Pagination
{
    public class PaginationParameters
    {
        //TODO validation
        public int? Page { get; set; }
        public int? PerPage { get; set; }
        public Sort Sort { get; set; }
    }

    public class Sort
    {
        public string ProperyName { get; set; }
        public SortDirection Direction { get; set; }
    }

    public enum SortDirection
    {
        Ascending,
        Descending
    }
}