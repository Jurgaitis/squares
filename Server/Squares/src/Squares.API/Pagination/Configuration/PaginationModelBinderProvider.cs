﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Squares.API.Pagination.Configuration
{
    public class PaginationModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context.BindingInfo.BinderType == typeof(PaginationModelBinder))
            {
                return new PaginationModelBinder();
            }

            return null;
        }
    }
}