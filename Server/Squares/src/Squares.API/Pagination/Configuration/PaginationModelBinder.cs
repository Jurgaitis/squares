﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Linq;

namespace Squares.API.Pagination.Configuration
{
    public class PaginationModelBinder : IModelBinder
    {
        //TODO mby move to injectable configuration
        private const string PAGE_KEY = "page";
        private const string PER_PAGE_KEY = "per_page";
        private const string SORT_KEY = "sort";
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            return Task.Run(() => BindModel(bindingContext));
        }

        private void BindModel(ModelBindingContext bindingContext)
        {
            bindingContext.BindingSource = BindingSource.Query;

            var pageStringValue = bindingContext.ValueProvider.GetValue(PAGE_KEY).Values.ToList().FirstOrDefault();
            var perPageStringValue = bindingContext.ValueProvider.GetValue(PER_PAGE_KEY).Values.ToList().FirstOrDefault();
            var sortStringValue = bindingContext.ValueProvider.GetValue(SORT_KEY).Values.ToList().FirstOrDefault();

            var model = MapToParameters(pageStringValue, perPageStringValue, sortStringValue);

            bindingContext.Result = ModelBindingResult.Success(model);
        }

        private PaginationParameters MapToParameters(string pageStringValue, string perPageStringValue,
            string sortStringValue)
        {

            var page = TryParse(pageStringValue);
            var perPage = TryParse(perPageStringValue);
            var sort = ParseSort(sortStringValue);

            return new PaginationParameters
            {
                Page = page,
                PerPage = perPage,
                Sort = sort
            };
        }

        private Sort ParseSort(string sortStringValue)
        {
            Sort sort = null;
            if (!string.IsNullOrEmpty(sortStringValue))
            {
                sort = new Sort
                {
                    Direction = SortDirection.Ascending
                };
                if (sortStringValue.StartsWith("-"))
                {
                    sort.Direction = SortDirection.Descending;
                    sort.ProperyName = sortStringValue.Substring(1);
                }
                else
                {
                    sort.ProperyName = sortStringValue;
                }
            }

            return sort;
        }

        //TODO move to utils
        private int? TryParse(string value)
        {
            int intValue;
            return int.TryParse(value, out intValue) ? (int?)intValue : null;
        }
    }
}