﻿using Squares.API;

namespace Microsoft.AspNetCore.Builder
{
    public static class APIApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseSquaresAPI(this IApplicationBuilder app)
        {
            app.UseWebSockets();
            app.UseMiddleware<WebsocketMiddleware>();

            app.UseMvc();

            return app;
        }
    }
}