﻿using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Squares.API.Conventions;
using Squares.API.Pagination.Configuration;
using Squares.API.TaskManager;
using Squares.API.TaskManager.Internal;
using Squares.API.TaskManager.Internal.Squares;
using Squares.Domain.Calculation.Squares;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class APIServiceCollectionExtensions
    {
        public static IServiceCollection AddSquaresAPI(this IServiceCollection services)
        {
            services.AddMvc(opts =>
                {
                    opts.ModelBinderProviders.Add(new PaginationModelBinderProvider());
                    opts.Conventions.Insert(0, new GlobalizedRoutePrefix("api"));
                })
                .AddApplicationPart(typeof(APIServiceCollectionExtensions).GetTypeInfo().Assembly);

            services.AddSquareCalculation();

            services.AddSingleton<ICalculationManager<Square>, SquaresCalculationManager>();

            return services;
        }
    }
}