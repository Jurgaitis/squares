﻿using System.Collections.Generic;
using Squares.Domain;

namespace Squares.Data
{
    internal interface IPointListCache
    {
        IEnumerable<PointsList> Get();
        void Cache(IEnumerable<PointsList> pointsLists);
    }
}