﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Squares.API.Pagination;
using Squares.API.ViewModels;
using Squares.API.ViewModels.Persistence;
using Squares.Domain;

namespace Squares.Data.Internal.ViewModels
{
    internal class PointListViewModelRepository : IPointListViewModelRepository
    {
        private IPointListCache _cache;

        public PointListViewModelRepository(IPointListCache cache)
        {
            _cache = cache;
        }

        public PageModel<PointListListViewModel> GetPage(PaginationParameters parameters)
        {
            IEnumerable<PointsList> pointsLists = _cache.Get();

            var modelsList = pointsLists.Select(list => new PointListListViewModel
            {
                Name = list.Name,
                Count = list.Points.Count
            });

            return GetPage(modelsList, parameters);
        }

        public PageModel<PointViewModel> GetPointsPage(string name, PaginationParameters parameters)
        {
            var pointsList = _cache.Get().SingleOrDefault(list => list.Name.Equals(name));

            if (pointsList == null)
            {
                return null;
            }

            IEnumerable<Point> points = pointsList.Points;
            var modelsList = points.Select(point => new PointViewModel
            {
                X = point.X,
                Y = point.Y
            });

            return GetPage(modelsList, parameters);
        }

        //TODO move to separate service
        private PageModel<T> GetPage<T>(IEnumerable<T> entities, PaginationParameters parameters)
        {
            var totalEntries = entities.Count();

            if (parameters.Sort != null)
            {
                var sortExpression = GetPropertySelector<T>(parameters.Sort.ProperyName);
                entities = parameters.Sort.Direction == SortDirection.Ascending
                    ? entities.OrderBy(sortExpression)
                    : entities.OrderByDescending(sortExpression);
            }
            if (parameters.Page.HasValue && parameters.PerPage.HasValue)
            {
                entities =
                    entities.Skip(((parameters.Page.Value - 1) * parameters.PerPage.Value))
                        .Take(parameters.PerPage.Value);
            }

            return new PageModel<T>
            {
                Entries = entities,
                TotalEntriesCount = totalEntries
            };
        }

        //TODO move to utils
        public static Func<T, object> GetPropertySelector<T>(string propertyName)
        {
            var arg = Expression.Parameter(typeof(T), "x");
            var property = Expression.Property(arg, propertyName);
            var conv = Expression.Convert(property, typeof(object));
            var exp = Expression.Lambda<Func<T, object>>(conv, arg);
            return exp.Compile();
        }
    }
}