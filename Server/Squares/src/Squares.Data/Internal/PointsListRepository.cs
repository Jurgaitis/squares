﻿using System.Collections.Generic;
using Squares.Domain;
using System.Linq;

namespace Squares.Data.Internal
{
    internal class PointsListRepository : IPointsListRepository
    {
        private readonly IPointListCache _cache;

        public PointsListRepository(IPointListCache cache)
        {
            _cache = cache;
        }

        public void Create(PointsList pointsList)
        {
            var alreadyCachedPointsLists = _cache.Get().ToList();

            var existingPointsList = alreadyCachedPointsLists.SingleOrDefault(list => list.Name.Equals(pointsList.Name));
            if (existingPointsList != null)
            {
                alreadyCachedPointsLists.Remove(existingPointsList);
            }

            alreadyCachedPointsLists.Add(pointsList);
            _cache.Cache(alreadyCachedPointsLists);
        }

        public PointsList Get(string name)
        {
            return _cache.Get().SingleOrDefault(pointsList => pointsList.Name.Equals(name));
        }

        public IEnumerable<PointsList> Get()
        {
            return _cache.Get();
        }

        public void Delete(string name)
        {
            var pointsLists = _cache.Get().ToList();
            pointsLists.RemoveAll(list => list.Name.Equals(name));

            _cache.Cache(pointsLists);
        }
    }
}