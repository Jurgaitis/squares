﻿using Microsoft.Extensions.DependencyInjection;
using Squares.API.ViewModels.Persistence;
using Squares.Data.Internal;
using Squares.Data.Internal.ViewModels;
using Squares.Domain;

namespace Squares.Data
{
    public static class PersistenceServiceCollectionExtensions
    {
        public static IServiceCollection AddSquaresPersistence(this IServiceCollection services)
        {
            var persistenceServicesLifetime = ServiceLifetime.Singleton;

            //Does not provide thread safety!!!
            //Separate cache instance only shared by repositories
            var cache = new PointListCache();

            services.Add(ServiceDescriptor.Describe(typeof(IPointsListRepository),
                provider => new PointsListRepository(cache),
                persistenceServicesLifetime));

            //TODO move to separate method
            services.Add(ServiceDescriptor.Describe(typeof(IPointListViewModelRepository),
                provider => new PointListViewModelRepository(cache),
                persistenceServicesLifetime));

            return services;
        }
    }
}