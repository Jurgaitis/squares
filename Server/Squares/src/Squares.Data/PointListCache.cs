﻿using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;
using Squares.Domain;

namespace Squares.Data
{
    internal class PointListCache : IPointListCache
    {
        private readonly IMemoryCache _cache = new MemoryCache(new MemoryCacheOptions());

        private const string CACHE_PREFIX = "points_list";

        public IEnumerable<PointsList> Get()
        {
            IEnumerable<PointsList> pointsLists;
            if (_cache.TryGetValue(CACHE_PREFIX, out pointsLists))
            {
                return pointsLists;
            }

            return new List<PointsList>();
        }

        public void Cache(IEnumerable<PointsList> pointsLists)
        {
            _cache.Set(CACHE_PREFIX, pointsLists);
        }
    }
}